package it.uniroma2.art.owlart.protegeimpl.model;

import edu.stanford.smi.protegex.owl.model.RDFResource;
import it.uniroma2.art.owlart.model.ARTResource;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 *
 */
public class ARTResourceProtegeImpl extends ARTNodeProtegeImpl implements ARTResource {

	public ARTResourceProtegeImpl(RDFResource res) {
		super(res);
	}
	
	public RDFResource getProtegeRDFResource() {
		return (RDFResource)node;
	}
	
}
