package it.uniroma2.art.owlart.protegeimpl;

public enum TripleType {
	AAA, AAO, SAA, APA, APO, SAO, SPA, SPO
}
