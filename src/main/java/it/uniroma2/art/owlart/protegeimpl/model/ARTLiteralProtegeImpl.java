package it.uniroma2.art.owlart.protegeimpl.model;

import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.protegeimpl.Protege2ARTResourceFactory;

import edu.stanford.smi.protegex.owl.model.RDFSLiteral;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 *
 */
public class ARTLiteralProtegeImpl extends ARTNodeProtegeImpl implements ARTLiteral {

	public ARTLiteralProtegeImpl(RDFSLiteral lit) {
		super(lit);
	}
	
	public ARTURIResource getDatatype() {
		//return ((Literal)node).getDatatype(); TODO serve prima la ResourceFactory
		return Protege2ARTResourceFactory.protegeResource2ARTURIResource(((RDFSLiteral)node).getDatatype());
	}

	public String getLabel() {
		return ((RDFSLiteral)node).getString();
	}

	public String getLanguage() {
		return (((RDFSLiteral)node).getLanguage());
	}
	
	public RDFSLiteral getLiteral() {
		return (RDFSLiteral)node;
	}

	/*
	public boolean equals(Object other) {
		
		if (this == other) {
			return true;
		}
		
		if (other instanceof ARTLiteral) {
			ARTLiteral otherLit = (ARTLiteral)other; 
			return ( this.getLabel().equals(otherLit.getLabel()) &&
					 this.getLanguage().equals(otherLit.getLanguage()) &&
					 this.getDatatype().equals(otherLit.getDatatype())
			);
		}
		
		return false;
	}
	*/
    
	/*
	public int hashCode() {
		return ((Literal)node).hashCode();
	}
	*/
}
