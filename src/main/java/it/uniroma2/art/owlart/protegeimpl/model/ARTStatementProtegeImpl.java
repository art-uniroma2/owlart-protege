package it.uniroma2.art.owlart.protegeimpl.model;

import edu.stanford.smi.protegex.owl.model.RDFObject;
import edu.stanford.smi.protegex.owl.model.triplestore.Triple;

import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 *
 */
public class ARTStatementProtegeImpl implements ARTStatement {

	Triple statement;
	
	public ARTStatementProtegeImpl(Triple stm) {
		this.statement = stm;
	}
	
	public ARTNode getObject() {
		return new ARTNodeProtegeImpl((RDFObject)statement.getObject());
	}

	public ARTURIResource getPredicate() {
		return new ARTURIResourceProtegeImpl(statement.getPredicate());
	}

	public ARTResource getSubject() {
		return new ARTResourceProtegeImpl(statement.getSubject());
	}

	public Triple getProtegeTriple() {
		return statement;
	}
	
	public String toString() {
		return statement.toString();
	}

	public ARTResource getNamedGraph() {
		
		throw new IllegalStateException("named graphs not supported for Protege implementation of OWL Art API");
		
		/*
		URI context = (URI)statement.getContext();
		if (context==null)
			return NodeFilters.MAINGRAPH;
		else
			return new ARTURIResourceProtegeImpl(context);
			*/
	}
	
}
