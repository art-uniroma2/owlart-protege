package it.uniroma2.art.owlart.protegeimpl.models.conf;

import it.uniroma2.art.owlart.models.conf.ModelConfigurationParameter;
import it.uniroma2.art.owlart.models.conf.PersistenceModelConfiguration;
import it.uniroma2.art.owlart.models.conf.RequiredConfigurationParameter;

/**
 * at the moment, there is no Protege Model configuration, so no parameters
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class ProtegeDBModelConfiguration extends ProtegeModelConfiguration implements
		PersistenceModelConfiguration {

	public static final String mySQLDriver = "com.mysql.jdbc.Driver";
	public static final String mySQLLocalBaseURL = "jdbc:mysql://127.0.0.1:3306/";

	@ModelConfigurationParameter(description = "jdbc driver for accessing the DB; defaults to: "
			+ mySQLDriver)
	public String dbDriver = mySQLDriver;

	@ModelConfigurationParameter(description = "url of the DB; defaults to: " + mySQLDriver)
	public String dbBaseUrl = mySQLLocalBaseURL;

	@ModelConfigurationParameter(description = "table name of the Protege model which is stored in the accessed DB")
	@RequiredConfigurationParameter
	public String dbTableName;

	@ModelConfigurationParameter(description = "user account name for DB credentials")
	@RequiredConfigurationParameter
	public String dbUserName;

	@ModelConfigurationParameter(description = "password for DB access credentials")
	@RequiredConfigurationParameter
	public String dbPassword;

	public ProtegeDBModelConfiguration() {
		super();
	}

	public String getShortName() {
		return "protege DB model configuration";
	}

	public boolean isPersistent() {
		return true;
	}

}
