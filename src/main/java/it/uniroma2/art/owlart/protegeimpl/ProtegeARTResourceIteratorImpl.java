package it.uniroma2.art.owlart.protegeimpl;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.RDFIteratorImpl;
import it.uniroma2.art.owlart.protegeimpl.model.ARTResourceProtegeImpl;

import java.util.Iterator;

import edu.stanford.smi.protegex.owl.model.RDFResource;


public class ProtegeARTResourceIteratorImpl extends RDFIteratorImpl<ARTResource> implements ARTResourceIterator {
	
	private Iterator<RDFResource> resIt;
    
    public ProtegeARTResourceIteratorImpl(Iterator<RDFResource> statIt) {
        this.resIt = statIt;
    }

	public void close() throws ModelAccessException {
		//do null
	}

	public ARTResource getNext() throws ModelAccessException {
		return new ARTResourceProtegeImpl(resIt.next());
	}

	public boolean streamOpen() throws ModelAccessException {
		return resIt.hasNext();
	}
	
	public String toString() {
		return resIt.toString();
	}

}
