package it.uniroma2.art.owlart.protegeimpl;

import java.util.Iterator;

import edu.stanford.smi.protegex.owl.model.triplestore.Triple;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.navigation.RDFIteratorImpl;
import it.uniroma2.art.owlart.protegeimpl.model.ARTStatementProtegeImpl;


public class ProtegeARTStatementIteratorImpl extends RDFIteratorImpl<ARTStatement> implements ARTStatementIterator {
	
	private Iterator<Triple> statIt;
    
    public ProtegeARTStatementIteratorImpl(Iterator<Triple> statIt) {
        this.statIt = statIt;
    }

	public void close() throws ModelAccessException {
		//do null
	}

	public ARTStatement getNext() throws ModelAccessException {
		return new ARTStatementProtegeImpl(statIt.next());
	}

	public boolean streamOpen() throws ModelAccessException {
		return statIt.hasNext();
	}
	
	public String toString() {
		return statIt.toString();
	}

}
