package it.uniroma2.art.owlart.protegeimpl.models.conf;

import it.uniroma2.art.owlart.models.conf.ModelConfigurationParameter;
import it.uniroma2.art.owlart.models.conf.PersistenceModelConfiguration;
import it.uniroma2.art.owlart.models.conf.RequiredConfigurationParameter;

/**
 * at the moment, there is no Protege Model configuration, so no parameters
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class ProtegeProjectModelConfiguration extends ProtegeModelConfiguration implements
		PersistenceModelConfiguration {

	@ModelConfigurationParameter(description = "name of the project file")
	@RequiredConfigurationParameter
	public String projectFileName;

	public ProtegeProjectModelConfiguration() {
		super();
	}

	public String getShortName() {
		return "protege Project model configuration";
	}

	public boolean isPersistent() {
		return false;
	}

}
