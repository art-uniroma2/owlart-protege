package it.uniroma2.art.owlart.protegeimpl.models.conf;

import it.uniroma2.art.owlart.models.conf.ModelConfigurationImpl;

/**
 * at the moment, there is no Protege Model configuration, so no parameters
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 *
 */
public abstract class ProtegeModelConfiguration extends ModelConfigurationImpl {

	public ProtegeModelConfiguration() {
		super();
	}

	public String getShortName() {
		return "protege model configuration";
	}

}
