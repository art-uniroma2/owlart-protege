package it.uniroma2.art.owlart.protegeimpl;

import java.util.Iterator;

import edu.stanford.smi.protegex.owl.model.triplestore.TripleStore;

import it.uniroma2.art.owlart.model.ARTNamespace;
import it.uniroma2.art.owlart.navigation.ARTNamespaceIterator;
import it.uniroma2.art.owlart.navigation.RDFIteratorImpl;

public class ProtegeARTNamespaceIteratorImpl extends RDFIteratorImpl<ARTNamespace> implements ARTNamespaceIterator {
	
	private Iterator<String> nsit;
	private TripleStore tripleStore;
	
	public ProtegeARTNamespaceIteratorImpl(Iterator<String> nsit, TripleStore tripleStore) {
		this.nsit = nsit;
		this.tripleStore = tripleStore;
	}
	
	public boolean streamOpen() throws it.uniroma2.art.owlart.exceptions.ModelAccessException {
		return nsit.hasNext();
	}

	public ARTNamespace getNext() throws it.uniroma2.art.owlart.exceptions.ModelAccessException {
		return new ProtegeARTNamespaceImpl(nsit.next(), tripleStore);
	}

    public void close() throws it.uniroma2.art.owlart.exceptions.ModelAccessException {
    	//do null
    }

}
