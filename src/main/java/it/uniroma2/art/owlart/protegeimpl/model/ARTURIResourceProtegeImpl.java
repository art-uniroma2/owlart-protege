package it.uniroma2.art.owlart.protegeimpl.model;

import it.uniroma2.art.owlart.model.ARTURIResource;

import edu.stanford.smi.protegex.owl.model.RDFResource;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 *
 */
public class ARTURIResourceProtegeImpl extends ARTResourceProtegeImpl implements ARTURIResource {

	public ARTURIResourceProtegeImpl(RDFResource res) {
		super(res);
	}
	
	public String getLocalName() {
		return ((RDFResource)node).getLocalName();
	}

	public String getNamespace() {
		return ((RDFResource)node).getNamespace();
	}

	public String getURI() {
		return ((RDFResource)node).getNamespace() + ((RDFResource)node).getLocalName();
	}
	
	// more immediate than the if on the ARTNodeProtegeImpl, so better to reimplement it
	public String toString() {
		return getURI();
	}
	
}
