package it.uniroma2.art.owlart.protegeimpl.model;

import edu.stanford.smi.protegex.owl.model.RDFResource;
import it.uniroma2.art.owlart.model.ARTBNode;

/**
 * not sure about this implementation. Uses {@link RDFResource#getName()} to implement the getID of blank
 * nodes. Do blank nodes exist in Protege OWL API?
 * 
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class ARTBNodeProtegeImpl extends ARTNodeProtegeImpl implements ARTBNode {

	public ARTBNodeProtegeImpl(RDFResource res) {
		super(res);
	}

	public RDFResource getProtegeRDFResource() {
		return (RDFResource) node;
	}

	public String getID() {
		return ((RDFResource) node).getName();
	}

	// more immediate than the if on the ARTNodeProtegeImpl, so better to reimplement it
    public String toString() {
        return "_:" + getID();
    }
}
