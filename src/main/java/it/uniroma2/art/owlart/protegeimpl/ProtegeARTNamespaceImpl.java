package it.uniroma2.art.owlart.protegeimpl;

import edu.stanford.smi.protegex.owl.model.triplestore.TripleStore;
import it.uniroma2.art.owlart.model.ARTNamespace;

public class ProtegeARTNamespaceImpl implements ARTNamespace {
    
    public ProtegeARTNamespaceImpl(String ns, TripleStore tripleStore) {
        this.namespace = ns;
    }
    
    String namespace;
    TripleStore tripleStore;
    
    public int hashCode() {
        return namespace.hashCode();
    }
    
    public String getName() {        
        return namespace;
    }

    public String getPrefix() {
    	return tripleStore.getPrefix(namespace);
    }

    public boolean equals(Object other) {
        return namespace.equals(other);
    }
    
    public String toString() {
        return namespace.toString();
    }
    
}
