package it.uniroma2.art.owlart.protegeimpl.model;

import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;

import edu.stanford.smi.protegex.owl.model.OWLModel;
import edu.stanford.smi.protegex.owl.model.triplestore.TripleStore;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 *
 */
public class ARTURIResourceEmptyImpl implements ARTURIResource {
	
	TripleStore tripleStore;
	String uri;
	OWLModel owlModel;
	
	public ARTURIResourceEmptyImpl(String uri, OWLModel owlModel) {
		//this.tripleStore = tripleStore;
		this.owlModel = owlModel;
		this.uri = uri;
		
	}
	
	public String getLocalName() {
		return owlModel.getLocalNameForURI(uri);
	}

	public String getNamespace() {
		return owlModel.getNamespaceForURI(uri);
	}

	public String getURI() {
		return uri;
	}
	
	/* 
	 * this at the moment excludes equality across resource belonging to different triple-store implementations
	 */
	public boolean equals(Object other) {
		
		if (this == other) {
			return true;
		}
		
		if (other instanceof ARTURIResourceEmptyImpl) {
			return this.uri.equals(((ARTURIResourceEmptyImpl)other).getURI());
		}
		
		if (other instanceof ARTNodeProtegeImpl) {
			if (((ARTNodeProtegeImpl)other).isURIResource()) {
				return (((ARTNodeProtegeImpl)other).asURIResource().getURI()).equals(this.getURI());
			}
		}
		
		return false;
	}
	
	public int hashCode() {
    	return uri.hashCode();
    }
	
	
	public String toString() {
		return uri;
	}
	
	
	public ARTLiteral asLiteral() {
		throw new IllegalStateException("cannot cast URI Resource to Literal");
	}

	public ARTResource asResource() {
		return this;
	}

	public ARTURIResource asURIResource() {
		return this;
	}
	
	public ARTBNode asBNode() {
		throw new IllegalStateException("cannot cast an URI Resource to a Blank Node");
	}

	public boolean isBlank() {
		return false;
	}

	public boolean isLiteral() {
		return false;
	}

	public boolean isResource() {
		return true;
	}

	public boolean isURIResource() {
		return true;
	}

	public String getNominalValue() {
		return getURI();
	}


	
}
