package it.uniroma2.art.owlart.protegeimpl;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.navigation.ARTNodeIterator;
import it.uniroma2.art.owlart.navigation.RDFIteratorImpl;
import it.uniroma2.art.owlart.protegeimpl.model.ARTNodeProtegeImpl;

import java.util.Iterator;

import edu.stanford.smi.protegex.owl.model.RDFObject;


public class ProtegeARTNodeIteratorImpl extends RDFIteratorImpl<ARTNode> implements ARTNodeIterator {
	
	private Iterator<RDFObject> resIt;
    
    public ProtegeARTNodeIteratorImpl(Iterator<RDFObject> statIt) {
        this.resIt = statIt;
    }

	public void close() throws ModelAccessException {
		//do null
	}

	public ARTNode getNext() throws ModelAccessException {
		return new ARTNodeProtegeImpl(resIt.next());
	}

	public boolean streamOpen() throws ModelAccessException {
		return resIt.hasNext();
	}
	
	public String toString() {
		return resIt.toString();
	}

}
