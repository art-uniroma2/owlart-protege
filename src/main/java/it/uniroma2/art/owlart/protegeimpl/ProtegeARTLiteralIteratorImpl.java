package it.uniroma2.art.owlart.protegeimpl;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.navigation.ARTLiteralIterator;
import it.uniroma2.art.owlart.navigation.RDFIteratorImpl;
import it.uniroma2.art.owlart.protegeimpl.model.ARTLiteralProtegeImpl;

import java.util.Iterator;

import edu.stanford.smi.protegex.owl.model.RDFSLiteral;


public class ProtegeARTLiteralIteratorImpl extends RDFIteratorImpl<ARTLiteral> implements ARTLiteralIterator {
	
	private Iterator<RDFSLiteral> resIt;
    
    public ProtegeARTLiteralIteratorImpl(Iterator<RDFSLiteral> statIt) {
        this.resIt = statIt;
    }

	public void close() throws ModelAccessException {
		//do null
	}

	public ARTLiteral getNext() throws ModelAccessException {
		//System.out.println("getting next");
		return new ARTLiteralProtegeImpl(resIt.next());
	}

	public boolean streamOpen() throws ModelAccessException {
		return resIt.hasNext();
	}
	
	public String toString() {
		return resIt.toString();
	}

}
