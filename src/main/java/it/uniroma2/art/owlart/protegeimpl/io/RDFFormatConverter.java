package it.uniroma2.art.owlart.protegeimpl.io;

import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;

import java.util.HashMap;

public class RDFFormatConverter {

	private static HashMap<RDFFormat, String> conversionMap;
	
	//from jena Model owldoc: supported languages are:
	//"RDF/XML", "RDF/XML-ABBREV", "N-TRIPLE", "TURTLE", (and "TTL") and "N3"
	
	static {
		 conversionMap = new HashMap<RDFFormat, String>();
		 conversionMap.put(RDFFormat.N3, RDFFormat.N3.getName());
		 conversionMap.put(RDFFormat.NTRIPLES, "N-TRIPLE");
		 conversionMap.put(RDFFormat.RDFXML, RDFFormat.RDFXML.getName());
		 conversionMap.put(RDFFormat.RDFXML_ABBREV, RDFFormat.RDFXML_ABBREV.getName());
		 //conversionMap.put(RDFFormat.TRIG, RDFFormat.TRIG.getName());
		 //conversionMap.put(RDFFormat.TRIX, RDFFormat.TRIX.getName());
		 conversionMap.put(RDFFormat.TURTLE, RDFFormat.TURTLE.getName());
	}
	
	
	public static String convert(RDFFormat format) throws UnsupportedRDFFormatException {
		String output = conversionMap.get(format);
		if (output == null)
			throw new UnsupportedRDFFormatException("format: " + format + " is not supported by current Protege OWLArt Implementation");
		else
			return output;
	}
	
}
