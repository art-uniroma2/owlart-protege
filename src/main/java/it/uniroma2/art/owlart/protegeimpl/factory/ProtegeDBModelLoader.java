package it.uniroma2.art.owlart.protegeimpl.factory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.stanford.smi.protege.model.Project;
import edu.stanford.smi.protegex.owl.database.OWLDatabaseKnowledgeBaseFactory;

/**
 * this class is used for all those cases in which a plain access to a ProtegeDB (using Protege original API)
 * has to be obtained.<br/>
 * access to ProtegeDB by using OWLART Protege Impl API can be obtained through the { @link
 * ARTModelFactoryProtegeImpl}
 * 
 * @author Armando Stellato <a href="mailto:stellato@info.uniroma2.it">stellato@info.uniroma2.it</a>;
 * 
 */
public class ProtegeDBModelLoader {

	protected static Logger logger = LoggerFactory.getLogger(ProtegeDBModelLoader.class);

	// these are public as they are shared by other utilities
	public final static String dbDriverProp = "dbDriver";
	public final static String dbBaseUrlProp = "dbBaseUrl";
	public final static String dbTableNameProp = "dbTableName";
	public final static String dbUserNameProp = "dbUserName";
	public final static String dbPasswordProp = "dbPassword";

	public Project loadProtegeProject(String dbDriver, String dbUrl, String dbTableName, String dbUserName,
			String dbPassword) {
		logger.info("creating Database Protege OWLModel: " + dbUrl);
		OWLDatabaseKnowledgeBaseFactory factory = new OWLDatabaseKnowledgeBaseFactory();
		Collection<Object> errors = new ArrayList<Object>();
		Project project = Project.createNewProject(factory, errors);
		OWLDatabaseKnowledgeBaseFactory.setSources(project.getSources(), dbDriver, dbUrl, dbTableName,
				dbUserName, dbPassword);
		project.createDomainKnowledgeBase(factory, errors, true);
		return project;
	}

	public Project loadProtegeProject(File propertyFile) throws IOException {
		Properties props = new java.util.Properties();
		FileReader fileReader = new FileReader(propertyFile);
		props.load(fileReader);
		String dbDriver = props.getProperty(dbDriverProp);
		String dbBaseUrl = props.getProperty(dbBaseUrlProp);
		String dbTableName = props.getProperty(dbTableNameProp);
		String dbUserName = props.getProperty(dbUserNameProp);
		String dbPassword = props.getProperty(dbPasswordProp);

		String dbUrl = dbBaseUrl + dbTableName + "?requireSSL=false&useUnicode=true&characterEncoding=UTF-8";

		return loadProtegeProject(dbDriver, dbUrl, dbTableName, dbUserName, dbPassword);
	}

	public Project loadProtegeProject(String configFileName) throws IOException {
		File configFile = new File(configFileName);
		return loadProtegeProject(configFile);
	}

}
