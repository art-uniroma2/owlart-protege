package it.uniroma2.art.owlart.protegeimpl;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.navigation.RDFIteratorImpl;
import it.uniroma2.art.owlart.protegeimpl.model.ARTURIResourceProtegeImpl;

import java.util.Iterator;

import edu.stanford.smi.protegex.owl.model.RDFProperty;


public class ProtegeARTPropertyIteratorImpl extends RDFIteratorImpl<ARTURIResource> implements ARTURIResourceIterator {
	
	private Iterator<RDFProperty> resIt;
    
    public ProtegeARTPropertyIteratorImpl(Iterator<RDFProperty> statIt) {
        this.resIt = statIt;
    }

	public void close() throws ModelAccessException {
		//do null
	}

	public ARTURIResource getNext() throws ModelAccessException {
		return new ARTURIResourceProtegeImpl(resIt.next());
	}

	public boolean streamOpen() throws ModelAccessException {
		return resIt.hasNext();
	}
	
	public String toString() {
		return resIt.toString();
	}

}
