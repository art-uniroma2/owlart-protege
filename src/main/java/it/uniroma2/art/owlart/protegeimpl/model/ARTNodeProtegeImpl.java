package it.uniroma2.art.owlart.protegeimpl.model;

import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;

import edu.stanford.smi.protegex.owl.model.RDFObject;
import edu.stanford.smi.protegex.owl.model.RDFResource;
import edu.stanford.smi.protegex.owl.model.RDFSLiteral;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class ARTNodeProtegeImpl implements ARTNode {

	RDFObject node;

	public ARTNodeProtegeImpl(RDFObject res) {
		this.node = res;
	}

	public RDFObject getProtegeRDFObject() {
		return node;
	}

	public ARTURIResource asURIResource() {
		return new ARTURIResourceProtegeImpl((RDFResource) node);
	}

	public ARTResource asResource() {
		return new ARTResourceProtegeImpl((RDFResource) node);
	}

	public ARTLiteral asLiteral() {
		return new ARTLiteralProtegeImpl((RDFSLiteral) node);
	}

	public ARTBNode asBNode() {
		return new ARTBNodeProtegeImpl((RDFResource) node);
	}

	public boolean isBlank() {
		if (!(node instanceof RDFResource) && !(node instanceof RDFSLiteral))
			return true;
		return false;
	}

	public boolean isLiteral() {
		return (node instanceof RDFSLiteral);
	}

	public boolean isURIResource() {
		if (node instanceof RDFResource)
			if (((RDFResource) node).getURI() != null)
				return true;
		return false;
	}

	public boolean isResource() {
		return (node instanceof RDFResource);
	}

	public String toString() {
		if (isURIResource())
			return ((RDFResource) node).getURI();
		if (isBlank())
			return "_:" + ((RDFResource) node).getName();
		// not sure the toString() of Protege is ok. But toString has already not been defined in OWLART for
		// literal nodes so, for the moment, I use this one
		return node.toString();
	}

	/*
	 * this at the moment excludes equality across resource belonging to different triple-store
	 * implementations
	 */
	public boolean equals(Object other) {

		// System.err.println("invoked equals between: " + other + " and " + this);

		if (this == other) {
			return true;
		}

		if (other instanceof ARTNodeProtegeImpl) {
			return this.node.equals(((ARTNodeProtegeImpl) other).node);
		}

		return false;
	}

	public int hashCode() {
		return node.hashCode();
	}

	public String getNominalValue() {
		if (node instanceof RDFResource) {
			if (((RDFResource) node).isAnonymous())
				// TODO not sure getName is appropriate. No time to check now :-)
				return ((RDFResource) node).getName();
			else
				return ((RDFResource) node).getURI();
		}
		// it's a literal
		return (String) ((RDFSLiteral) node).getPlainValue();

	}

}
