package it.uniroma2.art.owlart.protegeimpl.start;

import it.uniroma2.art.owlart.protegeimpl.factory.ARTModelFactoryProtegeImpl;
import it.uniroma2.art.owlart.protegeimpl.models.OWLModelProtegeImpl;

/**
 * generic interface to access a running OWLModelProtegeImpl. {@link ARTModelFactoryProtegeImpl} can actually
 * be used to perform such a task. This interface has however been thought for testing purposes, to access
 * OWLModelProtegeImpl instances which may come from different applications and loaded in different ways.
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 */
public interface ProtegeOWLARTLoader {

	OWLModelProtegeImpl getOWLModel();

}
