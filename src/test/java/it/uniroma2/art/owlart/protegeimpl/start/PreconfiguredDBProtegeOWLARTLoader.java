package it.uniroma2.art.owlart.protegeimpl.start;

import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.protegeimpl.factory.ARTModelFactoryProtegeImpl;
import it.uniroma2.art.owlart.protegeimpl.models.OWLModelProtegeImpl;
import it.uniroma2.art.owlart.protegeimpl.models.conf.ProtegeDBModelConfiguration;
import it.uniroma2.art.owlart.protegeimpl.models.conf.ProtegeModelConfiguration;

public class PreconfiguredDBProtegeOWLARTLoader implements ProtegeOWLARTLoader {

	// change these pars with your MySQL account
	protected static String dbusername = "root";
	protected static String dbpwd = "mysqlstarred";

	protected OWLModelProtegeImpl model = null;

	public PreconfiguredDBProtegeOWLARTLoader(String chosenDB) throws ModelCreationException {
		ARTModelFactoryProtegeImpl factImpl = new ARTModelFactoryProtegeImpl();
		OWLArtModelFactory<ProtegeModelConfiguration> fact = OWLArtModelFactory.createModelFactory(factImpl);
		ProtegeDBModelConfiguration conf = new ProtegeDBModelConfiguration();
		conf.dbTableName = chosenDB;
		conf.dbUserName = dbusername;
		conf.dbPassword = dbpwd;
		
		System.err.println(conf);
		
		model = (OWLModelProtegeImpl) fact.loadOWLModel(AGROVOC_INFO.agrovocBaseNS, "", conf);
	}

	public OWLModelProtegeImpl getOWLModel() {
		return model;
	}

}
