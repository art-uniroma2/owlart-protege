package it.uniroma2.art.owlart.protegeimpl.start;

import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.protegeimpl.factory.ARTModelFactoryProtegeImpl;
import it.uniroma2.art.owlart.protegeimpl.models.OWLModelProtegeImpl;
import it.uniroma2.art.owlart.protegeimpl.models.conf.ProtegeModelConfiguration;

import java.io.File;
import java.io.IOException;

public class FactoryBasedProtegeOWLARTLoader implements ProtegeOWLARTLoader {

	// change these pars with your MySQL account
	protected static String dbusername = "root";
	protected static String dbpwd = "mysqlstarred";

	protected OWLModelProtegeImpl model = null;

	public FactoryBasedProtegeOWLARTLoader(String uri, File propertyFile) throws IOException,
			ModelCreationException, UnsupportedModelConfigurationException, BadConfigurationException,
			UnloadableModelConfigurationException {
		ARTModelFactoryProtegeImpl factImpl = new ARTModelFactoryProtegeImpl();
		OWLArtModelFactory<ProtegeModelConfiguration> fact = OWLArtModelFactory.createModelFactory(factImpl);

		model = (OWLModelProtegeImpl) fact.loadOWLModel(uri, "",
				fact.createModelConfigurationObject(propertyFile));
	}

	public OWLModelProtegeImpl getOWLModel() {
		return model;
	}

}
