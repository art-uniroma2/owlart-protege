package it.uniroma2.art.owlart.protegeimpl.models;

import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.BaseRDFModelTestNoNG;
import it.uniroma2.art.owlart.models.BaseRDFTripleModel;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.protegeimpl.factory.ARTModelFactoryProtegeImpl;

import java.util.Iterator;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.stanford.smi.protegex.owl.model.triplestore.Triple;

/**
 * copied from basic Sesame2 implementation for unit tests of OWL ART API
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 *
 */
public class OWLModelTestProtegeImpl extends BaseRDFModelTestReadOnly {

	protected static Logger logger = LoggerFactory.getLogger(OWLModelTestProtegeImpl.class);
	static BaseRDFTripleModel model;
	
	@BeforeClass
	public static void loadRepository() throws Exception {
		model = BaseRDFModelTestReadOnly.initializeTest(new ARTModelFactoryProtegeImpl());
		ARTStatementIterator it = model.listStatements(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false);
		logger.info("triples listing:");
		while (it.streamOpen())
			logger.info(""+it.getNext());
		//localTests((BaseRDFModelProtegeImpl)model);
	}

	public static void localTests(BaseRDFModelProtegeImpl baseModel) throws Exception {		

	
		// PURE PROTEGE TESTS
		
		logger.info("\n\n\nSTART OF PURE PROTEGE TESTS\n");
		/*
		OWLModel owlModel = baseModel.getOWLModel();
		TripleStore tripleStore = baseModel.getTripleStore();
		TripleStoreModel tripleStoreModel = baseModel.getTripleStoreModel();
		
		Iterator<Triple> triplIt = tripleStore.listTriples();
		printTriples("list all triples", triplIt);
				
		triplIt = tripleStore.listTriplesWithSubject(owlModel.getRDFResource("localObjectA"));
		printTriples("list all triples with subject: localObjectA", triplIt);
		
		triplIt = tripleStore.listTriplesWithSubject(owlModel.getRDFResource("new:newObjA"));
		printTriples("list all triples with subject: new:newObjA", triplIt);

		//questo è per vedere se c'è uguaglianza tra uri e lnomi locali per stessi oggetti
		triplIt = tripleStore.listTriplesWithSubject(owlModel.getRDFResource("http://www.new.com#newObjA"));
		printTriples("list all triples with subject: new:newObjA", triplIt);
*/
		
		/*
		RDFResource pippo = owlModel.createRDFUntypedResource("http://pippo#objectA");
		tripleStore.add(pippo, owlModel.getRDFProperty("rdf:type"), owlModel.getRDFResource("localClassA"));
		tripleStore.add(pippo, owlModel.getRDFProperty("rdf:type"), owlModel.getRDFResource("localClassA"));
		triplIt = tripleStore.listTriplesWithSubject(owlModel.getRDFResource("http://pippo#objectA"));
		printTriples("list all triples with subject: http://pippo.it#objectA", triplIt);
		*/	
				/*
		tripleStore.add(((ARTResourceProtegeImpl)model.createURIResource("http://pippo#objectA")).getProtegeRDFResource(), owlModel.getRDFProperty("rdf:type"), owlModel.getRDFResource("localClassA"));
		triplIt = tripleStore.listTriplesWithSubject(((ARTURIResourceProtegeImpl)model.createURIResource("http://pippo#objectA")).getProtegeRDFResource());
		printTriples("list all triples with subject: http://pippo.it#objectA", triplIt);
		
		//questo recupera lòa tripla fatta prima da ART Model
		triplIt = tripleStore.listTriplesWithSubject(((ARTURIResourceProtegeImpl)model.createURIResource("http://mario#objectA")).getProtegeRDFResource());
		printTriples("list all triples with subject: http://mario#objectA (questo recupera la tripla di prima fatta da OWL ART", triplIt);
		
		triplIt = tripleStore.listTriples();
		printTriples("list all triples", triplIt);

		*/
		logger.info("\nEND OF PURE PROTEGE TESTS\n\n\n\n\n");
		
	}
	
	static void printTriples(String msg, Iterator<Triple> triplIt) {
		logger.info("\n\n"+msg);
		while(triplIt.hasNext()) logger.info("triple: " + triplIt.next());
	}
	
	
	@AfterClass
	public static void classTearDown() throws Exception {		
		try {
			BaseRDFModelTestNoNG.closeRepository();
		} catch (Exception e) {			
			System.err.println("failed to close the repository\n" + e);	
		}
	}
	
}
