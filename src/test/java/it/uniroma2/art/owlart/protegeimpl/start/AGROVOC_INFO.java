package it.uniroma2.art.owlart.protegeimpl.start;

public class AGROVOC_INFO {

	public static String genericAIMSBaseNS = "http://aims.fao.org/aos/";
	// used for all AGROVOC specific domain concepts
	public static String agrovocBaseNS = "http://aims.fao.org/aos/agrovoc/";
	// application concepts used by VOCBENCH
	public static String commonAIMBaseNS = "http://aims.fao.org/aos/common/";

	
	
	// FULL DBs
	protected static String agrovoc_full_20110223 = "agrovoc_wb_20110223";
	
	// SMALL DBs
	public static String agrovoc_small_20110223 = "agrovoc_wb_20110223_products";
	
	
	public static String current_agrovoc_full_DB = agrovoc_full_20110223;
	public static String current_agrovoc_small_DB = agrovoc_small_20110223;
	
}
